const MongoClient = require('mongodb').MongoClient;
console.log("Debug > running messengerdb.js...")
//const uri = "mongodb+srv://solomiankoj1:tO8TgjTOoHDj4bkQ@blockpartydb.hyv9h.mongodb.net/blockparty?retryWrites=true&w=majority";
const uri = "mongodb+srv://solomiankoj1:tO8TgjTOoHDj4bkQ@blockpartydb.hyv9h.mongodb.net/blockparty?retryWrites=true&w=majority"
const mongodbclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
let db = null;
mongodbclient.connect( (err,connection) => {
    if(err) throw err;
    console.log("Connected to the MongoDB cluster!");
    db = connection.db();
})
const dbIsReady = ()=>{
    return db != null;
};
const getDb = () =>{
    if(!dbIsReady())
        throw Error("No database connection");
    return db;
}

function validateUsername(username) {
    return (username && username.length > 4);
}

function validatePassword(password) {
    return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(password);
}

const checklogin = async (username,password)=>{
    //your implementation
    if(validateUsername(username) && validatePassword(password)) {
        var users = getDb().collection("users")
        var user = await users.findOne({username:username, password:password})
        if(user != null && user.username==username) {
            console.log("Debug > blockpartydb.checklogin -> user found:\n" + JSON.stringify(user))
            return true
        }
        return false
    }
    return false
}

const addUser = async (username,password)=>{
    if(validateUsername(username) && validatePassword(password)) {
        //your implementation
        console.log("Debug > messengerdb.addUser username:password is " + username + ":" + password)
        var users = getDb().collection("users");
        var user = await users.findOne({username:username});
        if(user != null && user.username == username) {
            console.log(`Debug > messengerdb.addUser: Username '${username}' exists!`);
            return "UserExists";
        }
        const newUser = {"username": username, "password": password}
        try {
            const result = await users.insertOne(newUser)
            if(result != null) {
                console.log("Debug > messengerdb.addUser: a new user added: \n", result);
                return "Success";
            }
        } catch {
            console.log("Debug > messengerdb.addUser: error for adding " + username + "\n", err);
            return "Error";
        }
    }
}

module.exports = {checklogin, addUser};