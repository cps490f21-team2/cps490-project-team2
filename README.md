# README.md CPS490 Report #

University of Dayton

Department of Computer Science

CPS 490 - Capstone I, FALL 2021

Instructor: Dr. Phu Phung


## Capstone I Project 


# The Messenger Application

Here is the link to our application: [Block Party](https://block-party-team2.herokuapp.com/)

# Team Members:

1. Asia Solomianko - <solomiankoj1@udayton.edu>
2. Katelyn Weidner - <weidnerk1@udayton.edu>
3. Mick Ward - <wardc11@udayton.edu>
4. Nathan Stull - <stulln2@udayton.edu>


# Project Management Information

Trello board (private access): <https://trello.com/b/TPu2hQxW/cps490f21-team2>

Bitbucket repository (private access): <https://bitbucket.org/cps490f21-team2/cps490-project-team2/src/master/>


## Revision History

| Date     |   Version     |  Description |
|----------|:-------------:|-------------:|
|09/07/2021|  0.1          |  Init draft  |
|10/03/2021|  0.2          |   Sprint 1   |
|10/27/2021|  0.3          |   Sprint 2   |


# Overview

The way our Messenger will be architected is that clients will be logging in and sendinging messages to the server, and the server will be taking these messages and sending them back to the clients. The server will also be storing these messages in a database so that they may be retrieved later. Here is a high-level architecture diagram:

![Architecture](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/613a69108dc12f67d4c74e14/previews/613a69118dc12f67d4c74e22/download/HighLevelArchitecture.png "High Level Architecture")



# System Analysis


## User Requirements

* Unregistered user can register as a user
* Registered user can:
	- login
	- send public chat messages
	- send private chat messages
	- create a group chat
	- send group chat messages
	- receive messages
	- see who is online
	- change their status to do not disburb, away, and online
	- clear the chats
	
## Use cases

Here is the diagram of our use cases. It is seen that the actors who are the registered users are our primary users of the Messenger. This shows us that these people are our primary customers and we should make sure the registered users are the ones who are authorized to use our Messenger.

![UseCase](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6179a047442ecd53d8fefcdd/download/UseCase.PNG "Use Case")

# System Design

_(Start from Sprint 1, keep updating)_
(General architectire, sequence/interaction diagrams for implemented use cases)
The way our Messenger will be architected is that all messages that come from the client side will be sent to the server. The server will then send the appropriate information to the proper client. 

At the login page, the user will type in a username. This information is sent to the server, and then server will send back the login information and allow the client to go to the 'main page,' which is the chatting page of our messenger. This happens with every interaction, like sending message. The user will click to send a message and the client sends the information to the server. The server will be taking these messages and sending them back to the proper client(s) using the message and possible username if the message is a private message. The messages are printed out on the client side so the user can look through all the messages received. Here is a high-level architecture diagram:


## Use-Case Realization

Registration interaction diagram
![RegistrationDiagram](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/617874bd782bf41fa1efd578/download/RegistrationInteractionDiagram.png)

Login interaction diagram
![LoginDiagram](https://trello.com/1/cards/614a3ef83bc0bb70fdf763dc/attachments/615a07efcca4503821da1889/download/LoginUseCaseDiagram.png "Login diagram")

User sending public message interaction diagram
![SendMessageUseCase](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/617870bdf087e92e4009e94c/download/PublicChatMessageInteractionDiagram.png "Send public message")

User sending private message interaction diagram
![SendPrivateMessageUseCase](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/617870bcc6982089dd36f27b/download/PrivateMessageInteractionDiagram.png "Send private message")

Creating a group chat interaction diagram
![GroupChatInteractionDiagram](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/6179ab5007d52012767a350c/download/CreateGroupChatInteraction.png "Create group chat interaction diagram")

Sending group chat message interaction diagram
![SendGroupMessageDiagram](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/6179ab51c7765714eba14316/download/GroupChatInteractionDiagram.png "Send group chat message diagram")

Status change interaction diagram
![StatusChangeDiagram](https://trello.com/1/cards/614a3ef83bc0bb70fdf763dc/attachments/615a07f1d642ac2e62a62932/download/StatusChangeUseCaseDiagram.png "Status change diagram")

Real-time typing interaction diagram
![RealTimeTyping](https://trello.com/1/cards/614a3ef83bc0bb70fdf763dc/attachments/615a07f0e75d3317638dea5f/download/RealTimeTypingIseCaseDiagram.png "Real-time typing")

Clear chat interaction diagram
![ClearChat](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/6178739340d698674e517a63/download/ClearChat.png "Clear chat")

Show/hide password interaction diagram
![Show/Hide Password](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/6178723013cc8c072cf1061e/download/ShowHidePassword.png "Show/Hide Password")


## Database 

_(Start from Sprint 3, keep updating)_

## User Interface

_(Start from Sprint 1, keep updating)_
The user opens the page and faces a login screen. Once the user types a username, he is able to send and receive public and private chat messages. 

Once logged in, the screen is separated in three parts. On the left there is the online users list. This contains any users who are currently logged in. A user can change their status to online (default), away, or do not disturb. In the middle of the page, the user sees a public chat box. When he sends a message, everyone online will receive the message. He will see when a user, including himself, is typing. On the left, there is the private and group chat box. The user can switch between the two types of chats. For the private chat box, the user can select who they want to send a private message to with a dropdown list. If a user is on do not disturb, he will not be able to receive messages. For the group chat box, the user can create a group chat and select which group they want to send it to. Clearing the chat and real time typing is implemented for the public, private, and group chats.

# Implementation

_(Start from Sprint 1, keep updating)_
### Login
At the login page, the user will type in a username. This information is sent to the server, and then server will send back the login information and allow the client to go to the 'main page,' which is the chatting page of our messenger.
![Visibility](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/615a01f60a09376517887072/download/UIChange.png "Visibility Change")

### Database 
Our messenger uses MongoDB as our database. Before adding users to the database, we make sure we check if the username already exists (code is found in Registration section below).
![Database code](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/617979fee03ac9740d316ebf/download/DatabaseCode.png "Databse code")

### Registration
On the login page, there is a link to register for an account under the login button. When clickin on that, the user is taken to a page where he can type in a username and password to register. The username must be between 4 and 16 characters, and the password should be between 8 and 20 characters. If an alert comes up and says it's successful, the user can go to the login page and login using the registered account.
![Registration code](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/617979ff0a43693f0841a450/download/RegistrationCode.png "Registration Code")

### Data validation
We check the text input to make sure we're expecting the right type of code. For out chat messages, we limit the number of characters to 262. We chose this number because it's the number of houses in the Student Neighborhood that are part of the energy program. Below we see an example for username/password inputs. 
![Username/pw validation](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/61797c2a59f3bc13070a56de/download/Validation.png "Validation")

### User list
On the left side of the page, the user list is displayed. This is used to update the private messaging dropdown as will be mentioned below. The user is set to 'online' by default when logging in. The user can toggle between online, away, and do not disturb. Note for private messaging: a user that is on do not disturb cannot be selected for private messaging.
![User list](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/615a01f93f6eae27f038c000/download/UserList.png "User list")

### Sending public messages
In the middle of the main page, there is the public chat. The user will type a message in the middle box and click the send button to send a message to the public chat. The client sends the information to the server. The server sends the message to everyone in the userlist, which contains users that are currently logged in (no matter if they are on do not disburb). Below we see the snippet of code to send messages to auhenticated (logged in) users.
![Public Messaging](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/61797ad3100fa106fd637b8c/download/PublicMessageCode.png "Public messaging")

### Sending private messages
To the right of the public message chat box, there is the private chat box. The client side will emit a message and a user to the server. The server will then emit this message to the specified user if he is in the user list. The server will send a message to the client saying that the message has been sent.
![Private Messaging](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/615a01f5e030c8547723a2ea/download/PrivateMessaging.png "Private messaging")

### Change status
The user can use the drop down on the left hand side of the page to change his status. This will notify other users whether the user is active and is open to messaging. Online is the default, away is for people who need to step away, and do not disturb doesn't allow people to private message them.
![Status change](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/615a035dedbdd82238b7dab1/download/UpdateStatus.png "Status change")

### Hide/Show Password
On the login and registration pages, the user can show or hide the password based on his preferences.
![Show/Hide Password](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/61797b30526a9743aa74a02b/download/ShowHidePW.png "Show and hide password")

### Clear chat
The user can clear the public and private chats with clicking the respective "clear" buttons
![Clear chat code](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/6179749098066b2fc84b4d0d/download/ClearChatCode.png "Clear chat code")

### Group chats
When selecting the group chat UI using the drop down on the right side of the page, the user can see group chats. There is a plus button where the user can create a new group chat. After typing the name of it and the users in the pop up, the group chat can then be created. When selecting that group, the user can type a message and send it to that group. Below we see our code for the creation of a group chat.
![Group chat creation code](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6179d8927174831d03917ef7/download/CreateGCcode.png "Group chat creation code")

<!--For each new sprint cycle, update the implementation of your system (break it down into subsections). It is helpful if you can include some code snippets to illustrate the implementation

Specify the development approach of your team, including programming languages, database, development, testing, and deployment environments. -->

# Evaluation

### Registration
The user can register a new username and password
![Registration](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6179a3d3fd434e13187ae2fc/download/UseCase_registeraccount.PNG)

### Show/Hide Password
The user can show/hide their password
![Show](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6179a3d837d96926eef047a3/download/UseCase_showpassword.PNG)
![Hide](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6179a3c62ddfb9069ba43f51/download/UseCase_hidepassword.PNG)

### Login
The user can login with their newly registered account
![Login](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6179a3dc93cb8016686751d3/download/UseCase_Userlogin.PNG)

### Clear Chat
The user can clear chats
![Chat_full](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6179a3b9ad51b970435c6cc1/download/UseCase_chatboxfull.PNG)
![Clear_chat](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6179a3bf1553d34edaba3879/download/UseCase_chatcleared.PNG)

### Logout
The user can logout
![Logout](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6179a3ced7aa657e5235643c/download/UseCase_LogoutAlert.PNG)

## Deployment

The messenger app is deployed through Heroku. We can create an app in the website and use Github to deploy it. Our messenger app can be accessed here: [Block Party](https://block-party-team2.herokuapp.com/). 



# Software Process Management


## Scrum process


![Sprint1](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6159fcae53bd555f015f28d6/download/Sprint1Gantt.png "Sprint 1")

![Sprint2](https://trello.com/1/cards/613a68ea550c7661cba77e1a/attachments/6159fcafbb7876186528c22f/download/Sprint2Gantt.png "Sprint 2")

![Sprint3](https://trello.com/1/cards/61787059f6a2b52ba6a6df21/attachments/6178791517f4ca033242b6dc/download/Sprint3GanttChart.png "Sprint 3")

### Sprint 0

Duration: 08/31/2021-09/09/2021

#### Completed Tasks: 

1. A PDF file contains:
	* The screenshot your team Trello board with Lists and cards in Sprints
	* The screenshot of the commits of your team repo
2. The PDF version of the main README.md. 
	* Information about the course, your team and members, and your team project (the Messenger)
	* Links to the team Trello board and Bitbucket repo
	* Overview: architecture figure + a short description of the system 
	* User requirements
	* Use cases:
		Use case diagram
		Each use case with
			Actor
			User stories
			Brief use case description
	* Project timeline with Gantt chart
	* Sprint 0 completion and contributions
3. Presentation slides in PDF

### Sprint 1

Duration: 09/10/2021-10/04/2021


#### Contributions: 

1.  Asia Solomianko, 23 hours, contributed in Trello, Google Slides, README, private messaging, real-time typing, clearing chat, sending messages to correct clients, data validation
2.  Katelyn Weidner, 16 hours, contributed in Trello, Google Slides, README, UI design
3.  Mick Ward, 25 hours, contributed in Trello, Google Slides, README, maintaining userlist,  setting online status, private messaging, data validation, group messaging
4.  Nathan Stull, 20 hours, contributed in Trello, Google Slides, README, real-time typing, enter to send message, delete message in chat box, show/hide password, date/time on messages sent


# User guide/Demo

Write as a demo with screenshots and as a guide for users to use your system.

_(Start from Sprint 1, keep updating)_