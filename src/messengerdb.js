const MongoClient = require('mongodb').MongoClient;
console.log("Debug > running messengerdb.js...")
const uri = "mongodb+srv://solomiankoj1:tO8TgjTOoHDj4bkQ@blockpartydb.hyv9h.mongodb.net/blockparty?retryWrites=true&w=majority"
const mongodbclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
let db = null;
mongodbclient.connect( (err,connection) => {
    if(err) throw err;
    console.log("Connected to the MongoDB cluster!");
    db = connection.db();
})
const dbIsReady = ()=>{
    return db != null;
};
const getDb = () =>{
    if(!dbIsReady())
        throw Error("No database connection");
    return db;
}

function validateUsername(username) {
    return (username && username.length > 4);
}

function validatePassword(password) {
    return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(password);
}

const checklogin = async (username,password)=>{
    //your implementation
    if(validateUsername(username) && validatePassword(password)) {
        var users = getDb().collection("users")
        var user = await users.findOne({username:username, password:password})
        if(user != null && user.username==username) {
            console.log("Debug > blockpartydb.checklogin -> user found:\n" + JSON.stringify(user))
            return true
        }
        return false
    }
    return false
}

const addUser = async (username,password) => {
    if(validateUsername(username) && validatePassword(password)) {
        //your implementation
        console.log("Debug > messengerdb.addUser username:password is " + username + ":" + password)
        var users = getDb().collection("users");
        var user = await users.findOne({username:username});
        if(user != null && user.username == username) {
            console.log(`Debug > messengerdb.addUser: Username '${username}' exists!`);
            return "UserExists";
        }
        const newUser = {"username": username, "password": password}
        try {
            const result = await users.insertOne(newUser)
            if(result != null) {
                console.log("Debug > messengerdb.addUser: a new user added: \n", result);
                return "Success";
            }
        } catch {
            console.log("Debug > messengerdb.addUser: error for adding " + username + "\n", err);
            return "Error";
        }
    }
}

const storePublicChat = (receiver, message) => {
    console.log("Debug > messengerdb.js > Storing Private message to MongoDB");
    let timestamp = Date.now();
    let chat = {receiver: receiver, message: message, timestamp: timestamp};
    try {
        const inserted = getDb().collection("public_chat").insertOne(chat);
        if(inserted != null){
            console.log("Debug > messengerdb.storePublicChat: a new chat message added: \n", JSON.stringify(chat));
        }
    } catch {
        console.log("Debug > messengerdb.storePublicChat: error for adding '" +
        JSON.stringify(chat) +"'\n");
    }
}

const storePrivateChat = (sender, receiver, message) => {
    console.log("Debug > messengerdb.js > Storing Private message to MongoDB");
    let timestamp = Date.now();
    let chat = {sender: sender, receiver: receiver, message: message, timestamp: timestamp};
    try {
        const inserted = getDb().collection("private_chat").insertOne(chat);
        if(inserted != null){
            console.log("Debug > messengerdb.storePrivateChat: a new chat message added: \n", JSON.stringify(chat));
        }
    } catch {
        console.log("Debug > messengerdb.storePrivateChat: error for adding '" +
        JSON.stringify(chat) +"'\n");
    }
}

const storeGroupChat = (sender, groupChatName, message) => {
    console.log("Debug > messengerdb.js > Storing Group message to MongoDB")
    let timestamp = Date.now()
    let chat = {sender: sender, receiver: groupChatName, message: message, timestamp: timestamp}
    try {
        const inserted = getDb().collection("group_chat").insertOne(chat);
        if(inserted != null){
            console.log("Debug > messengerdb.storeGroupChat: a new chat message added: \n", JSON.stringify(chat))
        }
    } catch {
        console.log("Debug > messengerdb.storeGroupChat: error for adding '" +
        JSON.stringify(chat) +"'\n")
    }
}

const loadChatHistory = async(limits=100) => {
    var chat_history = await getDb().collection("public_chat").find().sort({timestamp:-1}).limit(limits).toArray();
    //print debug info e.g., using JSON.stringify(chat_history)
    if (chat_history && chat_history.length > 0) 
        return chat_history
}


// {$and: [{receiver: receiver}, {sender:sender}]} OR {$and: [{receiver: sender}, {sender: receiver}]}
// {$or: [{$and: [{receiver: receiver}, {sender:sender}]}, {$and: [{receiver: sender}, {sender: receiver}]}]}

// {$and: [{receiver: receiver}, {sender:sender}]} OR {$and: [{receiver: sender}, {sender: receiver}]}

const loadPrivateChatHistory = async(receiver, sender, limits=100) => {
    var priv_chat_history = await getDb().collection("private_chat").find({$or: [{$and: [{receiver: receiver}, {sender:sender}]}, {$and: [{receiver: sender}, {sender: receiver}]}]}).sort({timestamp:-1}).limit(limits).toArray();
    //print debug info e.g., using JSON.stringify(chat_history)
    if (priv_chat_history && priv_chat_history.length > 0) 
        return priv_chat_history
}

const loadGroupChatHistory = async(receiver, limits=100) => {
    var group_chat_history = await getDb().collection("group_chat").find({receiver: receiver}).sort({timestamp:-1}).limit(limits).toArray();
    //print debug info e.g., using JSON.stringify(chat_history)
    if (group_chat_history && group_chat_history.length > 0) 
        return group_chat_history
}

module.exports = {checklogin, addUser, storePublicChat, storePrivateChat, storeGroupChat, loadChatHistory, loadPrivateChatHistory, loadGroupChatHistory};