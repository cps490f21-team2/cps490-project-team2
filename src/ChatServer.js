var http = require('http')
var express = require('express')
var blockpartydb = require('./messengerdb')
var app = express()
var server = http.createServer(app)
const port = process.env.PORT || 8080
const util = require('util');
var userlist = new Array();
var groupchatlist = new Array();
server.listen(port)
console.log(`Express HTTP Server is listening at port ${port}`)
app.use('/css', express.static('css'))
app.get('/', (request, response) => {
  console.log("Got an HTTP request")  
  response.sendFile(__dirname + '/index.html')
})

var DataLayer = { 
    info: 'Data Layer Implementation for Block Party messenger', // other code
    async checklogin(username, password) {
        var checklogin_result = await blockpartydb.checklogin(username, password)
        console.log("Debug > DataLayer.checklogin -> result = " + checklogin_result)
        return checklogin_result
    },
    async addUser(username, password){
        const result = await blockpartydb.addUser(username,password);
        return result;
    }
}

function validateUsername(username) {
    return (username && username.length > 4);
}

function validatePassword(password) {
    return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(password);
}

var io = require('socket.io')
var socketio = io.listen(server)
console.log("Socket.IO is listening at port: " + port)
socketio.on("connection", function (socketclient) {
    console.log("A new Socket.IO client is connected. ID = " + socketclient.id)
    

    /*
    * When a client is connected to, these are the events that could happen
    */

    // Login event listener
    socketclient.on("login", async (username, password) => {
        // NEED TO REVISE LINE BELOW (see if it is right, I added the password info)
        if(validateUsername && validatePassword ) {
            var welcomemessage = username + " has joined the chat system!"
            var checklogin = await DataLayer.checklogin(username, password)
            if(checklogin) {
                socketclient.username = username
                socketclient.authenticated = true
                socketclient.emit("welcomeUser")
                sendToAuthenticatedClient(socketclient, "welcome", welcomemessage)
                socketclient.emit("authenticated")
                var chat_history = await blockpartydb.loadChatHistory()
                if (chat_history && chat_history.length > 0) {
                    chat_history = chat_history.reverse()
                    //reverse the order as we get the latest first
                    socketclient.emit("chat_history", chat_history)
                }
                console.log(welcomemessage)
                const user = {username: username, password: password, status: "Online", id: socketclient.id}
                userlist.push(user)
                console.log("Debug > login - user list: ")
                console.log(userlist)
                socketio.emit("updateuserlist", userlist)
                socketio.emit("updategroupchatlist", groupchatlist)
                socketio.emit("updateuserstoaddtogroupchat", userlist)
                console.log("groupchat dropdown updated")
                console.log(groupchatlist)
            } else {
                socketclient.emit("invalid login")
            }
        } else {
            socketclient.emit("invalid login")
        }
    })

    socketclient.on("load_priv_chat", async (sender, receiver) => {
        var private_chat_history = await blockpartydb.loadPrivateChatHistory(sender, receiver)
        if (private_chat_history && private_chat_history.length > 0) {
            private_chat_history = private_chat_history.reverse()
            //reverse the order as we get the latest first
            socketclient.emit("priv_chat_history", private_chat_history)
            console.log("Debug > ChatServer.js sending priv_chat_history")   
        }
    })

    socketclient.on("load_group_chat", async (sender, groupName) => {
        var group_chat_history = await blockpartydb.loadGroupChatHistory(username)
        if (group_chat_history && group_chat_history.length > 0) {
            group_chat_history = group_chat_history.reverse()
            //reverse the order as we get the latest first
            socketclient.emit("group_chat_history", group_chat_history)
            console.log("Debug > ChatServer.js sending group_chat_messages")
        }
    })

    // Resigter event listener
    socketclient.on("register", async (username, password) => {
        if(validateUsername && validatePassword ) {
            const registration_result = await DataLayer.addUser(username, password);
            console.log("Debug -> register: got username:password " + username + ":" + password)
            socketclient.emit("registration", registration_result)
        } else {
            socketclient.emit("registration", "Invalid")
        }
    })

    // Chat event listener
    socketclient.on("chat", (message) => {
        var chatmessage = socketclient.username + " says: " + message
        console.log(chatmessage)
        sendToAuthenticatedClient(socketclient, "chat", chatmessage)
    })

    // Disconnect event listener
    socketclient.on('disconnect', function(){
        console.log(socketclient.client.conn.remoteAddress+":"+socketclient.id + ' is disconnected!')
        userlist = userlist.filter(user => user.id != socketclient.id)
        console.log("Debug > User list after disconnect: ")
        console.log(userlist)
        sendToAuthenticatedClient(socketclient, "updateuserlist", userlist)
    })

    // Logout
    socketclient.on('logout', function(){
        console.log(socketclient.client.conn.remoteAddress+":"+socketclient.id + ' has logged out!')
        userlist = userlist.filter(user => user.id != socketclient.id)
        socketclient.authenticated = false
        console.log("Debug > User list after logout: ")
        console.log(userlist)
        sendToAuthenticatedClient(socketclient, "updateuserlist", userlist)
    })
    
    // Status change event listener
    socketclient.on("statuschange", (status) => {        
        const userIndex = userlist.findIndex(user => user.id == socketclient.id)        
        userlist[userIndex].status = status
        sendToAuthenticatedClient(socketclient, "updateuserlist", userlist)
    })    

    socketclient.on("creategroupchat", (groupchat) => {
        groupchatlist.push(groupchat);
        sendToAuthenticatedClient(socketclient, "updategroupchatlist", groupchatlist)
        console.log("Debug > ChatServer.js > sending groupchat list to authenticated clients")
    })

    socketclient.on("sendgroupmessage", (groupname, message) => {
        console.log("GROUP MESSAGE SENDING")
        // need to see how we want the message displayed
        console.log(message)
        var chatmessage = socketclient.username + " says in " + groupname + ": " + message
        for (var index = 0; index < groupchatlist.length; index++) {
            // if the groupchatlist has the groupname of the group we want to send it to...
            console.log("Looking for " + groupname + ", found " + groupchatlist[index].groupname)
            if(groupchatlist[index].groupname == groupname) {
                for (var j = 0; j < groupchatlist[index].users.length; j++) {
                    var username = groupchatlist[index].users[j]
                    for (var k = 0; k < userlist.length; k++) {
                        // if the userlist has the username of the person we want to send it to...
                        console.log("Looking for " + username + ", found " + userlist[k].username)
                        if(userlist[k].username == username) { // this is hard coded in for now
                            // use the client ID to find the socket to send it to
                            var clientId = userlist[k].id
                            var sockets = socketio.sockets.sockets
                            for(var socketId in sockets) {
                                // if we find the clientID matching the socketID, send the message then break
                                if (socketId == clientId) {
                                    var socketToUse = sockets[socketId]
                                    console.log(chatmessage + "-> sent message to: " + username + ", " + clientId)
                                    blockpartydb.storeGroupChat(socketclient.username, groupname, data)
                                    socketToUse.emit("groupChat", chatmessage) // message the receiver will see
                                    // below is a message the sender will see
                                    break;
                                }        
                            }
                            break;
                        } else {
                            console.log("ERROR: user not found")
                        }
                    }
                }
            }
                
        }
    })

    // Public chat typing event listener
    socketclient.on("<TYPE>", function() {
        if (util.isNullOrUndefined(socketclient.username)) {return;}
        var msg= socketclient.username;
        sendToAuthenticatedClient(socketclient, "<TYPING>", msg);
        console.log("[<Typing>," + msg + "] is sent to all connected clients");
    })

    // Private chat typing event listener
    socketclient.on("<PRIV TYPE>", function(privateReceiver) {
        if (util.isNullOrUndefined(socketclient.username)) {return;}
        var msg= socketclient.username;
        
        // look for client ID corresponding to username
        for (var index = 0; index < userlist.length; index++) {
            // if the userlist has the username of the person we want to send it to...
            console.log("Looking for " + privateReceiver + ", found " + userlist[index].username)
            if(userlist[index].username == privateReceiver) { // this is hard coded in for now
                // use the client ID to find the socket to send it to
                var clientId = userlist[index].id;
                var sockets = socketio.sockets.sockets;
                for(var socketId in sockets) {
                    // if we find the clientID matching the socketID, send the message then break
                    if (socketId == clientId) {
                        sendToAuthenticatedClient(socketclient, "<PRIV TYPING>", msg);
                        console.log("[<PRIV TYPYING>," + msg + "] is sent to other client");
                        break;
                    }
                }
                break;
            }
        }
    })

    // Group chat typing event listener
    // NOT SURE IF THIS IS RIGHT BECAUSE GROUP RECEIVER IS MULTIPLE PEOPLE
    socketclient.on("<GROUP TYPE>", function(groupReceiver) {
        if (util.isNullOrUndefined(socketclient.username)) {return;}
        var msg= socketclient.username;
        
        // look for client ID corresponding to username
        for (var index = 0; index < userlist.length; index++) {
            // if the userlist has the username of the person we want to send it to...
            console.log("Looking for " + groupReceiver + ", found " + userlist[index].username)
            if(userlist[index].username == groupReceiver) { // this is hard coded in for now
                // use the client ID to find the socket to send it to
                var clientId = userlist[index].id;
                var sockets = socketio.sockets.sockets;
                for(var socketId in sockets) {
                    // if we find the clientID matching the socketID, send the message then break
                    if (socketId == clientId) {
                        sendToAuthenticatedClient(socketclient, "<PRIV TYPING>", msg);
                        console.log("[<PRIV TYPYING>," + msg + "] is sent to other client");
                        break;
                    }
                }
                break;
            }
        }
    })

    // Private chat sent event listener
    socketclient.on("privateChatSent", (message, username) => {
        console.log("Trying to get private chat to " + username);
        // need to see how we want the message displayed
        var chatmessage = socketclient.username + " says: " + message
        for (var index = 0; index < userlist.length; index++) {
            // if the userlist has the username of the person we want to send it to...
            console.log("Looking for " + username + ", found " + userlist[index].username)
            if(userlist[index].username == username) { // this is hard coded in for now
                // use the client ID to find the socket to send it to
                var clientId = userlist[index].id;
                var sockets = socketio.sockets.sockets;
                for(var socketId in sockets) {
                    // if we find the clientID matching the socketID, send the message then break
                    if (socketId == clientId) {
                        var socketToUse = sockets[socketId];
                        console.log(chatmessage + "-> sent message to: " + username + ", " + clientId)
                        blockpartydb.storePrivateChat(socketclient.username, username, message)
                        socketToUse.emit("privateChat", chatmessage); // message the receiver will see
                        // below is a message the sender will see
                        var chatForSender = "Private message to " + username + ", " + chatmessage;
                        socketclient.emit("privateChat", chatForSender);
                        break;
                    }        
                }
                break;
            } else {
                console.log("ERROR: user not found")
            }
        }
    })

})

function allClients() {
    console.log("Conected Clients:");
    var sockets = socketio.sockets.sockets;
    for(var socketId in sockets)
    {
        var socketclient = sockets[socketId];
        console.log(socketclient.client.conn.remoteAddress+":"+socketclient.id);
    }
}

function findClient(clientNeeded) {
    console.log("Looking for client: " + clientNeeded);
    var sockets = socketio.sockets.sockets;
    for(var socketId in sockets)
    {
        var socketclient = sockets[socketId];
        console.log(socketclient.client.conn.remoteAddress+":"+socketclient.id);
    }
}

function sendToAuthenticatedClient(senderSocket, type, data) {
    var sockets = socketio.sockets.sockets;
    for (var socketID in sockets) {
        var socketclient = sockets[socketID]
        if (socketclient.authenticated) {
            socketclient.emit(type, data)
            var logmsg = "Debug > sent to " + socketclient.username + " with ID = " + socketID
            console.log(logmsg)
            if(type === "chat") 
                blockpartydb.storePublicChat(socketclient.username, data)
            // else if(type === "")
            //     blockpartydb.storePrivateChat(socketclient.username, data) // not sure if this infor is correct
            // else if(type === "")
            //     blockpartydb.storeGroupChat(socketclient.username, data) // COULD BE LIST OF USERS THO
            // i font think private and public chats can be stored at this point either cause they don't use this method to send the message
        }
    }
}